using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;

[TestFixture]
public class NodeKeyTest
{
	[Test]
	public void EmptyKey ()
	{
		var key1 = new NodeKey ();
		var key2 = new NodeKey ();

		AssertEqual (key1, key2);
		Assert.IsEmpty (key1.Types);
	}

	[Test]
	public void SimpleKey ()
	{
		var key1 = new NodeKey (typeof(SimpleComponentA));
		var key2 = new NodeKey (typeof(SimpleComponentA));
		var key3 = new NodeKey ();

		AssertEqual (key1, key2);
		AssertNotEqual (key1, key3);
		AssertComponents (key1, typeof(SimpleComponentA));
	}
	
	[Test]
	public void DuplicateComponents ()
	{
		var key1 = new NodeKey (typeof(SimpleComponentA), typeof(SimpleComponentB));
		var key2 = new NodeKey (typeof(SimpleComponentA), typeof(SimpleComponentB), typeof(SimpleComponentA));

		AssertEqual (key1, key2);
		AssertComponents (key1, typeof(SimpleComponentA), typeof(SimpleComponentB));
	}
	
	[Test]
	public void ComplexKey ()
	{
		var key1 = new NodeKey (typeof(SimpleComponentA), typeof(SimpleComponentB));
		var key2 = new NodeKey (typeof(SimpleComponentB), typeof(SimpleComponentA));
		
		AssertEqual (key1, key2);
	}

	[Test]
	public void SimpleMatch()
	{
		throw new Exception ();
	}

	[Test]
	public void SimpleMismatch()
	{
		throw new Exception ();
	}

	[Test]
	public void ComplexMatch()
	{
		throw new Exception ();
	}

	[Test]
	public void ComplexMismatch()
	{
		throw new Exception ();
	}

	void AssertEqual (NodeKey key1, NodeKey key2)
	{
		Assert.AreEqual (key1, key2);
		Assert.AreEqual (key1.GetHashCode (), key2.GetHashCode ());
		Assert.True (key1 == key2);
		Assert.False (key1 != key2);
	}

	void AssertNotEqual (NodeKey key1, NodeKey key2)
	{
		Assert.AreNotEqual (key1, key2);
		Assert.AreNotEqual (key1.GetHashCode (), key2.GetHashCode ());
		Assert.False (key1 == key2);
		Assert.True (key1 != key2);
	}

	void AssertComponents (NodeKey key, params Type[] componentTypes)
	{
		Assert.AreEqual (key.Types.Length, componentTypes.Length);

		var list = new List<Type> (key.Types);
		foreach (var componentType in componentTypes) {
			Assert.True (list.Contains (componentType));
		}
	}
}
