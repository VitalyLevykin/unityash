using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[TestFixture]
public class NodesStorageTest
{
	NodesStorage storage;

	[SetUp]
	public void Init ()
	{
		storage = new NodesStorage ();
	}

	[Test]
	public void SimpleNodeList ()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		Assert.AreEqual (1, storage.GetNodeListsWithComponent (typeof(SimpleComponentA)).Count);
		Assert.AreEqual (key, storage.GetNodeListsWithComponent (typeof(SimpleComponentA)) [0].Key);

		NodeList nodeList = storage.GetNodeListsWithComponent (typeof(SimpleComponentA)) [0];

		Assert.AreEqual (1, nodeList.Systems.Count);
		Assert.Contains (system, nodeList.Systems);
	}

	[Test]
	public void MinimalNodeList ()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		var gameObject = new GameObject ();
		var nodeLists = AddComponentAndGetNodeLists<SimpleComponentA> (gameObject);

		Assert.AreEqual (1, nodeLists.Count);

		var nodeList = nodeLists [0];
		Assert.AreEqual (1, nodeList.GameObjects.Count);
		Assert.AreEqual (gameObject, nodeList.GameObjects.First());

		var sameNodeList = storage.GetNodeList (key);
		Assert.AreSame (nodeList, sameNodeList);
	}

	[Test]
	public void NoSystemNoNode()
	{
		var gameObject = new GameObject ();
		var nodeLists = AddComponentAndGetNodeLists<SimpleComponentA> (gameObject);

		Assert.IsEmpty (nodeLists);
	}

	[Test]
	public void AddComponent()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		var gameObject = new GameObject ();
		var nodeLists = AddComponentAndGetNodeLists<SimpleComponentA>(gameObject);

		Assert.AreEqual (1, nodeLists.Count);
		Assert.True (nodeLists[0].Systems.Contains(system));
	}

	[Test]
	public void RemoveComponent()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		var gameObject = CreateGameObjectWithComponents (typeof(SimpleComponentA));
		var nodeListsRemovedFrom = RemoveComponent<SimpleComponentA> (gameObject);

		var nodeList = storage.GetNodeList (key);
		Assert.IsEmpty (nodeList.GameObjects);

		Assert.AreEqual (1, nodeListsRemovedFrom.Count);
		Assert.True (nodeListsRemovedFrom[0].Systems.Contains(system));
	}

	[Test]
	[ExpectedException("AshException")]
	public void DuplicateComponent()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		var gameObject = CreateGameObjectWithComponents (typeof(SimpleComponentA));
		AddComponent<SimpleComponentA> (gameObject);
	}

	[Test]
	public void RepeatAddComponent()
	{
		var key = new NodeKey (typeof(SimpleComponentA));
		var system = new SystemInfo (new SimpleSystemA (), 0, key);
		storage.AddSystem (system);

		var gameObject = CreateGameObjectWithComponents (typeof(SimpleComponentA));
		RemoveComponent<SimpleComponentA> (gameObject);
		var nodeLists = AddComponentAndGetNodeLists<SimpleComponentA>(gameObject);

		Assert.AreEqual (1, nodeLists.Count);
		Assert.True (nodeLists[0].Systems.Contains(system));
	}

	[Test]
	public void MultipleNodeLists ()
	{
	}

	[Test]
	public void AddGameObject()
	{
	}

	[Test]
	public void RemoveGameObject()
	{
	}

	IList<NodeList> AddComponentAndGetNodeLists<T> (GameObject gameObject)
	{
		var component = gameObject.AddComponent (typeof(T));
		return storage.AddComponent (component);
	}

	T AddComponent<T> (GameObject gameObject)
	{
		var component = gameObject.AddComponent (typeof(T));
		storage.AddComponent (component);
		return (T)(object) component;
	}

	IList<NodeList> RemoveComponent<T> (GameObject gameObject)
	{
		var component = gameObject.GetComponent (typeof(T));
		GameObject.Destroy (component);
		return storage.RemoveComponent (component);
	}

	GameObject CreateGameObjectWithComponents(params Type[] types)
	{
		var gameObject = new GameObject ();
		foreach (Type type in types) {
			var component = gameObject.AddComponent (type);
			storage.AddComponent (component);
		}
		return gameObject;
	}
}
