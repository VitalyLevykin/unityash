using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[TestFixture]
public class SystemsStorageTest
{
	NodesStorage storage;

	[SetUp]
	public void Init ()
	{
		storage = new NodesStorage ();
	}

	[Test]
	public void EmptyStorage ()
	{
		Assert.IsEmpty (storage.GetNodeListsWithComponent (typeof(SimpleComponentA)));
		Assert.IsEmpty (storage.GetSystemsByNodeKey (new NodeKey ()));
		Assert.IsEmpty (storage.GetSystemsByNodeKey (new NodeKey (typeof(SimpleComponentA))));
		Assert.IsEmpty (storage.GetSystems ());
	}

	[Test]
	public void CleanAfterRemoveSystems ()
	{
		var system = new SystemInfo (new SimpleSystemA (), 0, new NodeKey (typeof(SimpleComponentA)));
		storage.AddSystem (system);
		storage.RemoveSystem (system.System);
		EmptyStorage ();
	}

	[Test]
	public void SimpleSystem ()
	{
		var system = new SystemInfo (new SimpleSystemA (), 0, new NodeKey (typeof(SimpleComponentA)));
		storage.AddSystem (system);

		Assert.AreEqual (1, storage.GetSystems ().Count);
		Assert.True (storage.GetSystems ().Contains (system));

		Assert.IsEmpty (storage.GetSystemsByNodeKey (new NodeKey ()));

		Assert.AreEqual (1, storage.GetSystemsByNodeKey (new NodeKey (typeof(SimpleComponentA))).Count);
		Assert.True (storage.GetSystemsByNodeKey (new NodeKey (typeof(SimpleComponentA))).Contains (system));
	}

	[Test]
	public void SystemPriority ()
	{
		var system1 = new SystemInfo (new SimpleSystemA (), 0, new NodeKey (typeof(SimpleComponentA)));
		var system2 = new SystemInfo (new SimpleSystemB (), 1, new NodeKey (typeof(SimpleComponentB)));
		var system3 = new SystemInfo (new EmptySystem (), 2, new NodeKey ());

		storage.AddSystem (system3);
		storage.AddSystem (system1);
		storage.AddSystem (system2);

		var systems = storage.GetSystems ();
		Assert.AreEqual (3, systems.Count);
		Assert.AreEqual (systems [0], system3);
		Assert.AreEqual (systems [1], system2);
		Assert.AreEqual (systems [2], system1);
	}

	[Test]
	public void SystemPriorityByComponentType ()
	{
		NodeKey key = new NodeKey (typeof(SimpleComponentA));
		var system1 = new SystemInfo (new SimpleSystemA (), 0, key);
		var system2 = new SystemInfo (new SimpleSystemB (), 1, key);
		var system3 = new SystemInfo (new EmptySystem (), 2, key);

		storage.AddSystem (system3);
		storage.AddSystem (system1);
		storage.AddSystem (system2);

		var systems = storage.GetSystemsByNodeKey (key);
		Assert.AreEqual (3, systems.Count);
		Assert.AreEqual (systems [0], system3);
		Assert.AreEqual (systems [1], system2);
		Assert.AreEqual (systems [2], system1);
	}

	[Test]
	public void RemoveSystem ()
	{
		var key1 = new NodeKey (typeof(SimpleComponentA));
		var key2 = new NodeKey (typeof(SimpleComponentB));
		var key3 = new NodeKey ();

		var system1 = new SystemInfo (new SimpleSystemA (), 0, key1);
		var system2 = new SystemInfo (new SimpleSystemB (), 1, key2);
		var system3 = new SystemInfo (new EmptySystem (), 2, key3);

		storage.AddSystem (system1);
		storage.AddSystem (system2);
		storage.AddSystem (system3);

		storage.RemoveSystem (system2.System);

		var systems = storage.GetSystems ();
		Assert.AreEqual (2, systems.Count);
		Assert.AreEqual (systems [0], system3);
		Assert.AreEqual (systems [1], system1);

		Assert.IsNotEmpty (storage.GetSystemsByNodeKey (key1));
		Assert.IsEmpty (storage.GetSystemsByNodeKey (key2));
		Assert.IsNotEmpty (storage.GetSystemsByNodeKey (key3));
	}

	[Test]
	public void ExistingNodesBeforeAddSystem()
	{
	}
}
