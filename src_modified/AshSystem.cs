using UnityEngine;

public class AshSystem : MonoBehaviour
{
	public virtual void NodeAdded(GameObject node)
	{
	}

	public virtual void NodeRemoved(GameObject node)
	{
	}

	public virtual void SystemUpdate(float dt)
	{
	}

	public virtual void NodeUpdate(GameObject node, float dt)
	{
	}
}
