using System.Collections.Generic;
using UnityEngine;

public class NodeList
{
	public NodeKey Key;
	public List<SystemInfo> Systems = new List<SystemInfo> (); // sorted
	public HashSet<GameObject> GameObjects = new HashSet<GameObject>();

	public NodeList (NodeKey key)
	{
		this.Key = key;
	}

	public bool AddIfMatches (GameObject gameObject)
	{
		if (Key.Matches (gameObject)) {
			GameObjects.Add (gameObject);
			return true;
		}
		return false;
	}
}
