using System.Collections.Generic;
using System.Reflection;
using System;

public class SystemInfo : IComparable
{
	public AshSystem System;
	public int Priority;
	public NodeKey NodeKey;
	public IEnumerable<FieldInfo> NodeParts;

	public SystemInfo (AshSystem system, int priority, NodeKey nodeKey)
	{
		this.System = system;
		this.Priority = priority;
		this.NodeKey = nodeKey;
	}

	public SystemInfo (AshSystem system, int priority, NodeKey nodeKey, IEnumerable<FieldInfo> nodeParts)
	{
		this.System = system;
		this.Priority = priority;
		this.NodeKey = nodeKey;
		this.NodeParts = nodeParts;
	}

	public int CompareTo(object other)
	{
		return Priority - ((SystemInfo)other).Priority;
	}
}
