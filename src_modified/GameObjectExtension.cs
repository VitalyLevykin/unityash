using System;
using UnityEngine;

public static class GameObjectExtension
{
	public static bool Initializing;

	public static T AddComponent<T>(this GameObject gameObject, Action<T> initializer) where T : MonoBehaviour
	{
		Initializing = true;
		try
		{
			T component = gameObject.AddComponent<T>();
			initializer(component);
			Engine.OnComponentEnabled(component);
			return component;
		}
		finally
		{
			Initializing = false;
		}
	}
}
