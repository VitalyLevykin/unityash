using System;
using UnityEngine;

public class AshComponent : MonoBehaviour
{
	void OnEnable()
	{
		if (GameObjectExtension.Initializing)
		{
			return;
		}
		Engine.OnComponentEnabled(this);
	}

	void OnDisable()
	{
		Engine.OnComponentDisabled(this);
	}
}
