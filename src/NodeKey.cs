using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class NodeKey
{
	public Type[] Types { get; private set; }
	
	public NodeKey (params Type[] types)
	{
		if (types.Contains (null)) {
			throw new ArgumentNullException ("types", "contains null");
		}
		
		Types = CollectionUtils.SortAndRemoveDuplicates (types);
	}
	
	public NodeKey (IEnumerable<FieldInfo> nodeParts)
	{
		var types = new List<Type> ();
		foreach (FieldInfo fieldInfo in nodeParts) {
			types.Add (fieldInfo.FieldType);
		}
		Types = CollectionUtils.SortAndRemoveDuplicates (types.ToArray ());
	}

	public bool Matches (GameObject gameObject)
	{
		return Types.All ((type) => gameObject.GetComponent (type) != null);
	}
	
	public override bool Equals (object obj)
	{
		if (obj == null) {
			return false;
		}
		if (ReferenceEquals (this, obj)) {
			return true;
		}
		if (obj.GetType () != typeof(NodeKey)) {
			return false;
		}
		
		NodeKey other = (NodeKey)obj;
		return Equals (other);
	}
	
	public bool Equals (NodeKey obj)
	{
		return Enumerable.SequenceEqual (Types, obj.Types);
	}
	
	public static bool operator == (NodeKey a, NodeKey b)
	{
		if (Object.ReferenceEquals (a, b)) {
			return true;
		}
		
		if (((object)a == null) || ((object)b == null)) {
			return false;
		}
		
		return a.Equals (b);
	}
	
	public static bool operator != (NodeKey a, NodeKey b)
	{
		return !(a == b);
	}
	
	public override int GetHashCode ()
	{
		unchecked {
			int hash = 17;
			
			foreach (Type key in Types) {
				hash = hash * 23 + ((key != null) ? key.GetHashCode () : 0);
			}
			
			return hash;
		}
	}
	
	public override string ToString ()
	{
		return string.Format ("[NodeKey: Keys={0}]", Types);
	}
}
