using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public class Engine : MonoBehaviour
{
	static Engine global;

	NodesStorage storage = new NodesStorage();
	bool addingScene;

	internal static void OnComponentEnabled(Component component)
	{
		if (global != null && !global.addingScene)
		{
			global.AddComponent(component);
		}
	}
	
	internal static void OnComponentDisabled(Component component)
	{
		if (global != null && !global.addingScene)
		{
			global.RemoveComponent(component);
		}
	}
	
	void Awake()
	{
		if (enabled)
		{
			StartScene();
		}
	}

	void StartScene()
	{
		Debug.Assert(global == null);

		//AddSystems();
		AddScene();
		global = this;
	}

//	void AddSystems()
//	{
//		var systems = GetComponents<AshSystem>();
//		foreach (var system in systems)
//		{
//			if (system.enabled)
//			{
//				AddSystem(system, 0);
//			}
//		}
//	}

	void AddScene()
	{
		ICollection<GameObject> gameObjects = new List<GameObject>();
		UnityUtils.CollectActiveGameObjects(ref gameObjects);

		addingScene = true;
		try
		{
			AddGameObjects(gameObjects);
		}
		finally
		{
			addingScene = false;
		}
	}

	public void AddSystem(AshSystem system, int priority)
	{
		Type systemType = system.GetType();
		IEnumerable<FieldInfo> nodeParts = CollectNodeParts(systemType);
		var key = new NodeKey(nodeParts);
		var systemInfo = new SystemInfo(system, priority, key, nodeParts);
		var nodeList = storage.AddSystem(systemInfo);

		foreach (var gameObject in nodeList.GameObjects)
		{
			system.NodeAdded(gameObject);
		}
	}

	public void RemoveSystem(AshSystem system)
	{
		var nodeList = storage.RemoveSystem(system);

		foreach (var gameObject in nodeList.GameObjects)
		{
			system.NodeRemoved(gameObject);
		}
	}

	public void AddComponent(Component component)
	{
		var nodeLists = storage.AddComponent(component);
		var notificationList = MakeNotificationList(nodeLists);

		foreach (var system in notificationList)
		{
			system.System.NodeAdded(component.gameObject);
		}
	}

	public void RemoveComponent(Component component)
	{
		var nodeLists = storage.RemoveComponent(component);
		var notificationList = MakeNotificationList(nodeLists);

		foreach (var system in notificationList)
		{
			system.System.NodeRemoved(component.gameObject);
		}
	}

	public void AddGameObject(GameObject gameObject)
	{
		var nodeLists = storage.AddGameObject(gameObject);
		var notificationList = MakeNotificationList(nodeLists);

		foreach (var system in notificationList)
		{
			system.System.NodeAdded(gameObject);
		}
	}

	public void AddGameObjects(IEnumerable<GameObject> gameObjects)
	{
		var systemToObjects = new SortedDictionary<SystemInfo, List<GameObject>>();

		foreach (var gameObject in gameObjects)
		{
			var nodeLists = storage.AddGameObject(gameObject);
			foreach (var nodeList in nodeLists)
			{
				foreach (var system in nodeList.Systems)
				{
					List<GameObject> gameObjectsList;
					if (!systemToObjects.TryGetValue(system, out gameObjectsList))
					{
						gameObjectsList = new List<GameObject>();
						systemToObjects.Add(system, gameObjectsList);
					}
					gameObjectsList.Add(gameObject);
				}
			}
		}

		foreach (var pair in systemToObjects)
		{
			var system = pair.Key;
			var gameObjectsList = pair.Value;
			foreach (var gameObject in gameObjectsList)
			{
				system.System.NodeAdded(gameObject);
			}
		}
	}

	public void RemoveGameObject(GameObject gameObject)
	{
		var nodeLists = storage.RemoveGameObject(gameObject);
		var notificationList = MakeNotificationList(nodeLists);

		foreach (var systemInfo in notificationList)
		{
			systemInfo.System.NodeRemoved(gameObject);
		}
	}

	public void Update()
	{
		float dt = Time.deltaTime;
		foreach (var system in storage.GetSystems())
		{
			system.System.SystemUpdate(dt);

			var key = system.NodeKey;
			var nodeList = storage.GetNodeList(key);
			foreach (var gameObject in nodeList.GameObjects)
			{
				InjectNodeParts(system.System, gameObject, system.NodeParts);
				system.System.NodeUpdate(gameObject, dt);
			}
		}
	}

	List<SystemInfo> MakeNotificationList(IList<NodeList> nodeLists)
	{
		List<SystemInfo> systems = new List<SystemInfo>(nodeLists.Count*2);
		foreach (var nodeList in nodeLists)
		{
			systems.AddRange(nodeList.Systems);
		}

		systems.Sort();
		return systems;
	}

	IEnumerable<FieldInfo> CollectNodeParts(Type systemType)
	{
		return systemType.GetFields().Where(field => field.IsDefined(typeof(NodePart), false));
	}

	void InjectNodeParts(AshSystem system, GameObject gameObject, IEnumerable<FieldInfo> nodeParts)
	{
		foreach (FieldInfo fieldInfo in nodeParts)
		{
			Component component = gameObject.GetComponent(fieldInfo.FieldType);
			Debug.Assert(component != null);

			fieldInfo.SetValue(system, component);
		}
	}
}
