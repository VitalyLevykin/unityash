using System;
using System.Collections.Generic;
using UnityEngine;

public class ComponentList
{
	public List<NodeList> NodeLists = new List<NodeList>();
	public HashSet<GameObject> GameObjects = new HashSet<GameObject>();
}
