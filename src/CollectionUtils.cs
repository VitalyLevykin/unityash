using System;
using System.Collections.Generic;

public class CollectionUtils
{
	public static T[] SortAndRemoveDuplicates<T> (T[] array)
	{
		Array.Sort (array, (x, y) => x.GetHashCode () - y.GetHashCode ());
		List<T> list = null;
		for (int i = 1; i < array.Length; i++) {
			if (Object.Equals (array [i], array [i - 1])) {
				if (list == null) {
					list = new List<T> (array.Length);
					for (int j = 0; j < i; j++) {
						list.Add (array [j]);
					}
				}
			} else if (list != null) {
				list.Add (array [i]);
			}
		}
		return list != null ? list.ToArray () : array;
	}
}
