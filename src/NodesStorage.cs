using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NodesStorage
{
	Dictionary<NodeKey, NodeList> NodeLists = new Dictionary<NodeKey, NodeList>();
	Dictionary<Type, ComponentList> ComponentLists = new Dictionary<Type, ComponentList>();
	List<SystemInfo> Systems = new List<SystemInfo>();
	// sorted
	public NodeList GetNodeList(NodeKey key)
	{
		NodeList nodeList = null;
		NodeLists.TryGetValue(key, out nodeList);
		return nodeList;
	}

	public IList<NodeList> GetNodeListsWithComponent(Type componentType)
	{
		ComponentList nodeLists;
		if (ComponentLists.TryGetValue(componentType, out nodeLists))
		{
			return nodeLists.NodeLists.AsReadOnly();
		}
		return new List<NodeList>().AsReadOnly();
	}

	public IList<SystemInfo> GetSystemsByNodeKey(NodeKey key)
	{
		NodeList nodeList = GetNodeList(key);
		if (nodeList != null)
		{
			return nodeList.Systems.AsReadOnly();
		}
		return new List<SystemInfo>();
	}
	// sorted
	public IList<SystemInfo> GetSystems()
	{
		return Systems.AsReadOnly();
	}

	public NodeList AddSystem(SystemInfo system)
	{
		Debug.Assert(!Systems.Contains(system));

		NodeList nodeList = GetNodeList(system.NodeKey);
		if (nodeList == null)
		{
			nodeList = CreateNodeList(system.NodeKey);
		}

		InsertToSortedList(system, nodeList.Systems);
		InsertToSortedList(system, Systems);
		return nodeList;
	}

	public NodeList RemoveSystem(AshSystem ashSystem)
	{
		int systemIndex = FindSystemInfo(ashSystem);
		Debug.Assert(systemIndex != -1);

		SystemInfo system = Systems[systemIndex];
		NodeList nodeList = GetNodeList(system.NodeKey);
		Debug.Assert(nodeList != null);
		
		RemoveFromSortedList(system, nodeList.Systems);
		Systems.RemoveAt(systemIndex);

		if (nodeList.Systems.Count == 0)
		{
			RemoveNodeList(nodeList);
		}
		return nodeList;
	}

	public IList<NodeList> AddGameObject(GameObject gameObject)
	{
		var nodeLists = new List<NodeList>();

		foreach (Component component in gameObject.GetComponents<Component>())
		{
			if (UnityUtils.ComponentEnabled(component))
			{
				var nodeListsAdded = AddComponent(component);
				nodeLists.AddRange(nodeListsAdded);
			}
		}

		return nodeLists;
	}

	public IList<NodeList> RemoveGameObject(GameObject gameObject)
	{
		List<NodeList> nodeLists = new List<NodeList>();

		foreach (Component component in gameObject.GetComponents<Component>())
		{
			if (UnityUtils.ComponentEnabled(component))
			{
				var nodeListsRemoved = RemoveComponent(component);
				nodeLists.AddRange(nodeListsRemoved);
			}
		}

		return nodeLists;
	}

	public IList<NodeList> AddComponent(Component component)
	{
		Debug.Assert(UnityUtils.ComponentEnabled(component));

		GameObject gameObject = component.gameObject;
		Type componentType = component.GetType();
		ComponentList componentList = RegisterComponentIfNot(componentType);

		Debug.Assert(!componentList.GameObjects.Contains(gameObject));
		componentList.GameObjects.Add(gameObject);

		List<NodeList> matchedNodeLists = new List<NodeList>();
		foreach (NodeList nodeList in componentList.NodeLists)
		{
			Debug.Assert(!nodeList.GameObjects.Contains(gameObject));

			if (nodeList.AddIfMatches(gameObject))
			{
				matchedNodeLists.Add(nodeList);
			}
		}

		return matchedNodeLists;
	}

	public IList<NodeList> RemoveComponent(Component component)
	{
		Debug.Assert(UnityUtils.ComponentEnabled(component));

		Type componentType = component.GetType();
		GameObject gameObject = component.gameObject;

		Debug.Assert(IsComponentRegistered(componentType));
		Debug.Assert(ComponentLists[componentType].GameObjects.Contains(gameObject));

		ComponentList componentList = ComponentLists[componentType];
		bool removed = componentList.GameObjects.Remove(gameObject);

		Debug.Assert(removed);

		List<NodeList> nodeListsRemovedFrom = new List<NodeList>();
		foreach (NodeList nodeList in componentList.NodeLists)
		{
			if (nodeList.GameObjects.Remove(gameObject))
			{
				nodeListsRemovedFrom.Add(nodeList);
			}
		}

		if (componentList.GameObjects.Count == 0 && componentList.NodeLists.Count == 0)
		{
			UnregisterComponent(componentType);
		}

		return nodeListsRemovedFrom;
	}

	NodeList CreateNodeList(NodeKey nodeKey)
	{
		var nodeList = new NodeList(nodeKey);
		AddNodeList(nodeList);
		return nodeList;
	}

	void AddNodeList(NodeList nodeList)
	{
		Debug.Assert(!NodeLists.ContainsKey(nodeList.Key));

		NodeLists[nodeList.Key] = nodeList;

		foreach (Type componentType in nodeList.Key.Types)
		{
			ComponentList componentList = RegisterComponentIfNot(componentType);

			Debug.Assert(!componentList.NodeLists.Contains(nodeList));
			componentList.NodeLists.Add(nodeList);
		}
	}

	void RemoveNodeList(NodeList nodeList)
	{
		Debug.Assert(NodeLists.ContainsKey(nodeList.Key));

		foreach (Type componentType in nodeList.Key.Types)
		{
			ComponentList componentList = ComponentLists[componentType];
			bool removed = componentList.NodeLists.Remove(nodeList);

			Debug.Assert(removed);

			if (componentList.NodeLists.Count == 0)
			{
				UnregisterComponent(componentType);
			}
		}

		NodeLists.Remove(nodeList.Key);
	}

	bool IsComponentRegistered(Type componentType)
	{
		return ComponentLists.ContainsKey(componentType);
	}
	//	void RegisterComponent (Type componentType)
	//	{
	//		Debug.Assert (!IsComponentRegistered (componentType));
	//		ComponentLists.Add (componentType, new List<NodeList> ()); // TODO
	//	}
	ComponentList RegisterComponentIfNot(Type componentType)
	{
		ComponentList componentList;
		if (!ComponentLists.TryGetValue(componentType, out componentList))
		{
			componentList = new ComponentList();
			ComponentLists.Add(componentType, componentList);
		}
		return componentList;
	}

	void UnregisterComponent(Type componentType)
	{
		ComponentLists.Remove(componentType);
	}

	void InsertToSortedList(SystemInfo systemInfo, List<SystemInfo> systems)
	{
		int index = FindIndexToInsert(systems, systemInfo.Priority);
		systems.Insert(index, systemInfo);
	}

	int FindIndexToInsert(List<SystemInfo> systems, int priority)
	{
		for (int i = 0; i < systems.Count; i++)
		{
			if (systems[i].Priority <= priority)
			{
				return i;
			}
		}
		return systems.Count;
	}

	void RemoveFromSortedList(SystemInfo system, List<SystemInfo> systems)
	{
		int index = systems.BinarySearch(system);
		systems.RemoveAt(index);
	}

	int FindSystemInfo(AshSystem ashSystem)
	{
		for (int i = 0; i < Systems.Count; i++)
		{
			if (Systems[i].System == ashSystem)
			{
				return i;
			}
		}
		return -1;
	}
}
