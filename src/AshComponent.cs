using System;
using UnityEngine;

public class AshComponent : MonoBehaviour
{
	void OnEnable()
	{
		Engine.OnComponentEnabled(this);
	}

	void OnDisable()
	{
		Engine.OnComponentDisabled(this);
	}
}
