using System;
using UnityEngine;
using System.Collections.Generic;

public static class UnityUtils
{
	public static void CollectActiveGameObjects(ref ICollection<GameObject> collector)
	{
		foreach (var gameObject in UnityEngine.Object.FindObjectsOfType<GameObject>())
		{
			if (gameObject.activeInHierarchy)
			{
				collector.Add(gameObject);
			}
		}
	}

	public static void CollectActiveGameObjects(GameObject root, ref ICollection<GameObject> collector)
	{
		if (root.activeInHierarchy)
		{
			collector.Add(root);

			foreach (GameObject child in root.transform)
			{
				CollectActiveGameObjects(child, ref collector);
			}
		}
	}
	
	public static bool ComponentEnabled(Component component)
	{
		return !(component is Behaviour) || ((Behaviour)component).enabled;
	}
}
