using UnityEngine;

public class AshSystem
{
	public virtual void NodeAdded (GameObject node)
	{
	}

	public virtual void NodeRemoved (GameObject node)
	{
	}

	public virtual void Update (float dt)
	{
	}

	public virtual void NodeUpdate (GameObject node, float dt)
	{
	}
}
