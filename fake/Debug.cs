using System;
using System.Diagnostics;

public class Debug
{
	[Conditional("DEBUG")] 
	public static void Assert(bool value)
	{
		if (!value)
			throw new AshException ();
	}
}
