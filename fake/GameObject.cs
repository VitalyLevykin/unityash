using System;
using System.Collections.Generic;

namespace UnityEngine
{
	public class GameObject
	{
		Dictionary<Type, Component> Components = new Dictionary<Type, Component>();

		public static void Destroy(Component component)
		{
			component.gameObject.Components.Remove (component.GetType());
		}

		public Component AddComponent (Type type)
		{
			Debug.Assert (!Components.ContainsKey (type));

			Component component = (Component)type.GetConstructor(new Type[] { }).Invoke(new object[] { });
			component.gameObject = this;
			Components.Add(type, component);

			return component;
		}

		public T AddComponent<T> ()
		{
			return (T)(object) AddComponent (typeof(T));
		}

		public Component GetComponent (Type type)
		{
			Component component;
			if (Components.TryGetValue (type, out component)) {
				return component;
			}
			return null;
		}

		public T GetComponent<T> ()
		{
			return (T)(object)GetComponent (typeof(T));
		}

		public T[] GetComponents<T> ()
		{
			List<T> list = new List<T> ();
			foreach (Component component in Components.Values) {
				if (component is T)
					list.Add ((T)(object)component);
			}
			return list.ToArray ();
		}
	}
}
