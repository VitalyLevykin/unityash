namespace UnityEngine
{
	public class Component
	{
		public GameObject gameObject;

		public T[] GetComponents<T>() where T : Component
		{
			return gameObject.GetComponents<T> ();
		}
	}
}
